package com.romssilva.smartupnp.smartupnp;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by romssilva on 2018-05-31.
 */

public class FavoritesManager {
    private static FavoritesManager ourInstance;

    private ArrayList<DeviceDisplay> devices;
    private Context context;

    private final String FAVORITES = "favorites";
    private final String SHARED_PREFS_FILE = "favorites_file";

    public static FavoritesManager getInstance(Context context) {
        if (null == ourInstance) {
            ourInstance = new FavoritesManager(context);
        }
        return ourInstance;
    }

    private FavoritesManager(Context context) {
        devices = new ArrayList<>();
        this.context = context;

        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFS_FILE, Context.MODE_PRIVATE);

        devices = (ArrayList<DeviceDisplay>) ObjectSerializer.deserialize(prefs.getString(FAVORITES, ObjectSerializer.serialize(new ArrayList<DeviceDisplay>())));
    }

    public void addDevice(DeviceDisplay deviceDisplay) {
        devices.add(deviceDisplay);

        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFS_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(FAVORITES, ObjectSerializer.serialize(devices));
        editor.commit();
    }

    public void removeDevice(DeviceDisplay deviceDisplay) {
        devices.remove(deviceDisplay);

        SharedPreferences prefs = context.getSharedPreferences(SHARED_PREFS_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(FAVORITES, ObjectSerializer.serialize(devices));
        editor.commit();
    }

    public boolean isFavorite(DeviceDisplay deviceDisplay) {
        return devices.contains(deviceDisplay);
    }

    public List<DeviceDisplay> getFavorites() {
        return devices;
    }
}
