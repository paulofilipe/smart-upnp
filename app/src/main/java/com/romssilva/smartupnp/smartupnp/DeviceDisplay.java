package com.romssilva.smartupnp.smartupnp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.romssilva.smartupnp.smartupnp.activities.DeviceActivity;

import org.fourthline.cling.model.meta.Device;

/**
 * Created by romssilva on 2018-04-08.
 */

public class DeviceDisplay {

    public static final String FROM_SHAKE = "FROM_SHAKE";
    private Device device;

    public DeviceDisplay(Device device) {
        this.device = device;
    }

    public Device getDevice() {
        return device;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeviceDisplay that = (DeviceDisplay) o;
        return device.equals(that.device);
    }

    @Override
    public int hashCode() {
        return device.hashCode();
    }

    @Override
    public String toString() {
        String name =
                getDevice().getDetails() != null && getDevice().getDetails().getFriendlyName() != null
                        ? getDevice().getDetails().getFriendlyName()
                        : getDevice().getDisplayString();
        // Display a little star while the device is being loaded (see performance optimization earlier)
        return device.isFullyHydrated() ? name : name + " *";
    }

    public void startIntent(Context context, boolean fromShake) {
        if (getDevice().getDetails().getPresentationURI() != null) {
            String appPackageName = getDevice().getDetails().getPresentationURI().toString();
            Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(appPackageName);
            if (launchIntent != null) {
                launchIntent.putExtra(FROM_SHAKE, fromShake);
                context.startActivity(launchIntent);//null pointer check in case package name was not found
            } else {
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
        } else {
            Intent intent = new Intent(context, DeviceActivity.class);
            intent.putExtra("device_udn", getDevice().getIdentity().getUdn().getIdentifierString());
            intent.putExtra(FROM_SHAKE, fromShake);
            context.startActivity(intent);
        }
    }
}
